# The idea is to simulate the DAVID implementation made for Max in Python using Praat
import numpy as np
from parselmouth import Sound
from parselmouth.praat import call
import os
import matplotlib.pyplot as plt
from scipy import interpolate

reference_tone = 100.5

def cent2herz(ct, base = reference_tone):
    """Converts deviation in cents to a value in Hertz"""
    st = ct / 100
    semi1 = np.log(np.power(2, 1 / 12))
    return (np.exp(st * semi1) * base)

def pitch_shift(manipulation, shift_ct):
    pitch_tier = call(manipulation, "Extract pitch tier")
    shift_st = shift_ct / 100
    call(pitch_tier, "Shift frequencies", sound.xmin, sound.xmax, shift_st, "semitones")
    call([pitch_tier, manipulation], "Replace pitch tier")
    return manipulation

def vibrato(manipulation, vibrato_rand_rate, vibrato_depth=None, vibrato_rate=None):
    # Replace duration tier
    duration_tier = call("Create DurationTier", "tmp", 0, dur)
    call([duration_tier], "Add point", 0, 1)
    call([duration_tier, manipulation], "Replace duration tier")

    # Create point process
    pitch = call(sound, "To Pitch", man_step_size, man_min_F0, man_max_F0)
    pointprocess = call(pitch, "To PointProcess")
    matrix = call(pointprocess, "To Matrix")
    r = vibrato_rand_rate/100000

    formula = "self + randomGauss(0, %f)" % r
    if vibrato_depth is not None and vibrato_rate is not None:
        formula += " + %f * sin(2*pi*%f*x)" % (vibrato_depth, vibrato_rate)

    call([matrix], "Formula", formula)

    pointprocess2 = call(matrix, "To PointProcess")
    call([pointprocess2, manipulation], "Replace pulses")
    return manipulation

def inflection(manipulation, points, start_time, duration_ms = 500, plot=True):
    # Extract pitch tier and load it into numpy
    # TODO find a better way to avoid saving it to a txt file first..
    pitch_tier = call(manipulation, "Extract pitch tier")
    pitch_tier.save_as_short_text_file('tmp.PitchTier')
    pt_matrix = np.loadtxt('tmp.PitchTier', skiprows=6)
    os.remove('tmp.PitchTier')

    # Convert all sound measures to seconds
    duration_s = duration_ms/1000
    if not all([p >= 0 and p <= duration_ms for p in points[0]]):
        raise ValueError('Time must lay in specified duration')
    time_kernel = [start_time + p/1000 for p in points[0]]


    # Convert cents to hertz
    pitch_kernel = [cent2herz(p) - reference_tone for p in points[1]]

    pitch = pt_matrix[1::2]
    time = pt_matrix[0::2]
    samp_rate = np.median(np.diff(time))

    idxs = [i for i, t in enumerate(time) if t >= start_time and t < start_time + duration_s]
    time = time[idxs]
    pitch = pitch[idxs]

    tck = interpolate.splrep(time_kernel, pitch_kernel, s=0)
    pitch_kernel_spline = interpolate.splev(time, tck, der=0)

    new_pitch_values = pitch + pitch_kernel_spline
    if plot:
        plt.figure()
        plt.plot(time_kernel, pitch_kernel, 'g', time, pitch, 'r', time, pitch_kernel_spline, 'y', time, new_pitch_values)
        plt.legend(['Original contour', 'Linear points', 'Spline interpolation', 'New contour'])
        plt.show()

    time_margin = samp_rate/10
    time_min = min(time) - time_margin
    time_max = max(time) + time_margin
    # Make sure the pitch Tier is empty
    call(pitch_tier, "Remove points between", time_min, time_max)
    for i in range(len(pitch_kernel_spline)):
        call(pitch_tier, "Add point", time[i], new_pitch_values[i])
    call([pitch_tier, manipulation], "Replace pitch tier")
    return manipulation

def filter(type, gain, freq=80000):
    raise NotImplementedError("I don't know how to implement a ")


sound = Sound('demo.wav')
dur = sound.xmax - sound.xmin
man_step_size=0.01
man_min_F0=75
man_max_F0=600

# Inflection demo
points = np.stack((
    np.array([0, 58.510628, 255.319138, 401.595734, 500]),
    np.array([-200, 140, 82.666664, 0, 0])
))
manipulation = call(sound, "To Manipulation", man_step_size, man_min_F0, man_max_F0)
manipulation = inflection(manipulation, points, 0.43)
sound_inf = call(manipulation, "Get resynthesis (overlap-add)")
call(sound_inf, "Save as WAV file", 'demo_inf.wav')

# Pitch shift test
manipulation = call(sound, "To Manipulation", man_step_size, man_min_F0, man_max_F0)
manipulation = pitch_shift(manipulation, 1000)
sound_100ct = call(manipulation, "Get resynthesis (overlap-add)")
call(sound_100ct, "Save as WAV file", 'demo_+1000ct.wav')

# Vibrato test
manipulation = call(sound, "To Manipulation", man_step_size, man_min_F0, man_max_F0)
manipulation = vibrato(manipulation, 500)
sound_vibrato40 = call(manipulation, "Get resynthesis (overlap-add)")
call(sound_vibrato40, "Save as WAV file", 'demo_sound_vibrato500.wav')

manipulation = call(sound, "To Manipulation", man_step_size, man_min_F0, man_max_F0)
manipulation = vibrato(manipulation, 40, 248, 8.5)
sound_vibrato40 = call(manipulation, "Get resynthesis (overlap-add)")
call(sound_vibrato40, "Save as WAV file", 'demo_sound_vibrato_test.wav')



